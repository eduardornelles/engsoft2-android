package engsoft2.com.br.spotted2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by EduDornelles on 15/10/2015.
 */
public class FeedAdapter extends ArrayAdapter<Post>{

    private LruCache<String, Bitmap> memoryCache;
    private HashMap<String, String> idPairs = new HashMap<String, String>();
    ImageButton btn_comentar;
    ImageButton btn_like;
    ImageButton btn_dislike;
    ImageButton btn_denunciar;
    View customView;
    TimelineFragment timeline;

    FeedAdapter(Context context, ArrayList<Post> posts, TimelineFragment timeline){
        super( context, R.layout.feed_row, posts);
        this.timeline = timeline;
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        memoryCache = new LruCache<String, Bitmap>(cacheSize)  {
            @Override
            protected int sizeOf(String key, Bitmap bitmap){
                return (bitmap.getRowBytes() * bitmap.getHeight()) / 1024; //don't use getByteCount for API < 12
            }
        };

    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        Log.e( "KEY FOTO", key );
        if (getBitmapFromMemCache(key) == null) {
            memoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return memoryCache.get(key);
    }

    public View getView(int position, View convertView, ViewGroup parent){

        LayoutInflater inflater = LayoutInflater.from(getContext());

        final Post post = getItem(position);

        customView = inflater.inflate(R.layout.feed_row, parent, false);
        TextView text = (TextView) customView.findViewById( R.id.post_texto );
        TextView tags = (TextView) customView.findViewById( R.id.tags_post );
        TextView data = (TextView) customView.findViewById( R.id.post_data );
        ImageView imagem = (ImageView) customView.findViewById(R.id.post_image);
        imagem.setScaleType(ImageView.ScaleType.CENTER);
        TextView qteLike = (TextView) customView.findViewById(R.id.qte_like);
        TextView qteDislike = (TextView) customView.findViewById(R.id.qte_dislike);
        TextView qteComentarios = (TextView) customView.findViewById(R.id.qte_comentar);
        btn_comentar = (ImageButton) customView.findViewById(R.id.btn_comentar);
        btn_like = (ImageButton) customView.findViewById(R.id.btn_like);
        btn_dislike = (ImageButton) customView.findViewById(R.id.btn_dislike);
        btn_denunciar = (ImageButton) customView.findViewById(R.id.btn_denunciar);
        if( post.getTexto().length() > 160){
            text.setText( String.valueOf( post.getTexto().toCharArray(), 0, 160 ) + "..." );
        }else{
            text.setText( post.getTexto() );
        }

        if( post.isDenunciado() == false ){
            btn_denunciar.setAlpha((float) 0.5);
        }

        if( post.isLiked() == false ){
            btn_like.setAlpha((float) 0.5);
        }

        if( post.isDisliked() == false ){
            btn_dislike.setAlpha((float) 0.5);
        }

        btn_comentar.setAlpha((float) 0.5);

        data.setText(post.getData());
        qteLike.setText( String.valueOf(post.getLikes()) );
        qteDislike.setText( String.valueOf( post.getDislikes() ) );
        qteComentarios.setText(String.valueOf(post.getQteComentarios()));
        tags.setText( post.getTags() );

        if( post.getImagemUrl() != null && post.getImagemUrl() != "" ){
           new DownloadImageTask( imagem )
                   .execute(post.getImagemUrl());
        }else{
           imagem.setVisibility( View.GONE );
        }

        btn_comentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeline.openComentarios( post );
            }

        });

        btn_like.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
            Log.e("LIKED", String.valueOf( post.isLiked() ) );
            if( post.isLiked() == false ) {
                like(post);
                post.setLiked(true);
                post.setDisliked(false);
            }
            }

        });

        btn_dislike.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.e("DISLIKED", String.valueOf( post.isDisliked() ) );
            if( post.isDisliked() == false ){
                dislike( post );
                post.setDisliked(true);
                post.setLiked(false);
            }
            }

        });

        btn_denunciar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Log.e("DENUNCIADO", String.valueOf( post.isDenunciado() ) );
                if( post.isDenunciado() == false ){
                    denunciar(post);
                    post.setDenunciado(true);
                }
            }

        });

        return customView;

    }

    public void updateAdapter(){
        this.notifyDataSetChanged();
    }

    public void like( Post post){
        String url = "http://noiadigital.com.br/spotted/public/api/post/like";

        try {

            final Post postInner = post;

            Map<String, String>  params = new HashMap<>();
            params.put("usuario_id", String.valueOf( post.getUsuario_id() ) );
            params.put("post_id", String.valueOf(post.getId()));

            HttpRequest.getInstance( getContext() ).somePostRequestReturningString(url, params, new HttpRequest.SomeCustomListener<String>() {
                @Override
                public void getResult(String result) {
                    if (!result.isEmpty()) {

                        try {
                            JSONObject obj = new JSONObject( result );
                            Log.e("RESULT", obj.toString() );
                            postInner.setLikes( obj.getInt("likes") );
                            postInner.setDislikes(obj.getInt("dislikes"));
                            postInner.setLiked(obj.getBoolean("liked"));
                            postInner.setDisliked(obj.getBoolean("disliked"));
                            updateAdapter();
                            Toast.makeText(getContext(), "Você curtiu o post", Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Toast.makeText(getContext(), "Não foi possível curtiu o post", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch( Exception e){
            Toast.makeText( getContext() , "Erro ao conectar ao servidor.", Toast.LENGTH_SHORT).show();
        }
    }

    public void dislike( Post post ){
        String url = "http://noiadigital.com.br/spotted/public/api/post/dislike";

        Map<String, String>  params = new HashMap<>();
        // the POST parameters:
        try {

            params.put("usuario_id", String.valueOf( post.getUsuario_id() ) );
            params.put("post_id", String.valueOf( post.getId() ) );

            final Post postInner = post;

            HttpRequest.getInstance( getContext() ).somePostRequestReturningString(url, params, new HttpRequest.SomeCustomListener<String>() {
                @Override
                public void getResult(String result) {
                    if (!result.isEmpty()) {
                        try {
                            JSONObject obj = new JSONObject( result );
                            Log.e("RESULT", obj.toString() );
                            postInner.setLikes( obj.getInt("likes") );
                            postInner.setDislikes(obj.getInt("dislikes"));
                            postInner.setLiked(obj.getBoolean("liked"));
                            postInner.setDisliked(obj.getBoolean("disliked"));
                            //atualizar adapter
                            updateAdapter();
                            Toast.makeText(getContext(), "Você descurtiu o post", Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }else{
                        Toast.makeText( getContext() , "Não foi possível descurtir o post", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch( Exception e){
            Toast.makeText( getContext() , "Erro ao conectar ao servidor.", Toast.LENGTH_SHORT).show();
        }
    }

    public void denunciar( Post post ){
        String url = "http://noiadigital.com.br/spotted/public/api/post/denunciar";

        Map<String, String>  params = new HashMap<>();
        // the POST parameters:
        try {
            params.put("usuario_id", String.valueOf( post.getUsuario_id() ) );
            params.put("post_id", String.valueOf( post.getId() ) );

            final Post postInner = post;

            HttpRequest.getInstance( getContext() ).somePostRequestReturningString(url, params, new HttpRequest.SomeCustomListener<String>() {
                @Override
                public void getResult(String result) {
                    if (!result.isEmpty()) {
                        try {
                            JSONObject obj = new JSONObject( result );
                            postInner.setDenunciado(true);
                            //atualizar adapter
                            updateAdapter();
                            Toast.makeText(getContext(), "Você denunciou o post", Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }else{
                        Toast.makeText( getContext() , "Não foi possível denunciar o post", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch( Exception e){
            Toast.makeText( getContext() , "Erro ao conectar ao servidor.", Toast.LENGTH_SHORT).show();
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            String[] urlSplit = urldisplay.split("/");
            String keyCache = urlSplit[ urlSplit.length-1 ];
            Bitmap mIcon11 = getBitmapFromMemCache( keyCache );

            if( mIcon11 == null ){

                try {
                    InputStream in = new java.net.URL(urldisplay).openStream();
                    mIcon11 = BitmapFactory.decodeStream(in);
                } catch (Exception e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
                addBitmapToMemoryCache(keyCache, mIcon11);
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }



}
