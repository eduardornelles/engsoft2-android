package engsoft2.com.br.spotted2;

/**
 * Created by efd_net on 16/10/2015.
 */
public class Data {

    private String foobar = null;

    public boolean isReady() {
        return (foobar != null);
    }

    public String getFoobar() { return foobar; }

    public void setFoobar(String foobar) { this.foobar = foobar; }

    @Override
    public String toString() {
        return "Data [Foobar=" + foobar + "]";
    }

}
