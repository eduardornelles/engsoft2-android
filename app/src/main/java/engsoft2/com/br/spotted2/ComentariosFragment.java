package engsoft2.com.br.spotted2;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.Profile;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComentariosFragment extends android.support.v4.app.Fragment {

    View v;
    EditText comentario;
    String post_id;
    String usuario_id;
    ListView feed_list;
    ComentariosAdapter adapter;
    ArrayList<Comentario> listaComentarios;

    public ComentariosFragment( ) {

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.comentarios, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                toBack();
                return true;
            case R.id.action_refresh:
                getComentarios();
                return true;
            case R.id.action_send:
                sendComentario();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void toBack(){
        getFragmentManager().popBackStack();
    }

    @Override
    public void onCreate( Bundle savedInstance ){
        super.onCreate(savedInstance);
        getComentarios();
    }

    @Override
    public void onResume(){
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    toBack();
                    return true;
                }
                return false;

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);

        Bundle b = getArguments();
        post_id = b.getString("post_id");
        usuario_id = b.getString("usuario_id");

        View  v = inflater.inflate(R.layout.fragment_comentarios, container, false);
        comentario = (EditText) v.findViewById( R.id.comentario );

        listaComentarios = new ArrayList<>();
        adapter = new ComentariosAdapter(getContext() , listaComentarios );
        feed_list = (ListView) v.findViewById( R.id.lista_comentarios );
        feed_list.setAdapter(adapter);

        return v;
    }


    public void sendComentario(){


        String url = "http://noiadigital.com.br/spotted/public/api/post/insert_comentario";

        if( comentario.getText().toString().trim().length() < 1){
            new AlertDialog.Builder(getContext())
                    .setTitle("Atenção!")
                    .setMessage("Informe o conteúdo de seu comentário.")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                        }
                    })
                    .show();
        }else {

            final ProgressDialog ringProgressDialog = ProgressDialog.show(getContext(), "Aguarde ...", "Enviando comentário ...", true);
            ringProgressDialog.setCancelable(true);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Result handling
                            try {
                                JSONObject json = new JSONObject(response);
                                JSONArray comentarios = json.getJSONArray("comentarios");
                                refreshAdapter(comentarios);

                                comentario.setText("");

                                ringProgressDialog.dismiss();

                            } catch (JSONException e) {
                                ringProgressDialog.dismiss();
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    ringProgressDialog.dismiss();

                    new AlertDialog.Builder(getContext())
                            .setTitle("Erro!")
                            .setMessage("Não foi possível conectar ao servidor.")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .show();

                }
            }) {

                @Override
                protected Map<String, String> getParams() {

                    SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                    long login_id = sharedPref.getInt("login_id", 0);

                    Map<String, String> params = new HashMap<>();
                    // the POST parameters:
                    params.put("usuario_id", String.valueOf( login_id ));
                    params.put("post_id",  post_id );
                    params.put("texto", comentario.getText().toString());

                    return params;
                }
            };

            // Add the request to the queue
            Volley.newRequestQueue(getContext()).add(stringRequest);
        }
    }

    public void refreshAdapter( JSONArray comentarios ){
        try {
            adapter.clear();
            listaComentarios.clear();

            for (int i = 0; i < comentarios.length(); i++) {
                JSONObject comentario = comentarios.getJSONObject(i);
                Integer id = (int) comentario.get("id");
                int usuario_id = comentario.getInt("usuario_id");
                int post_id = comentario.getInt("post_id");
                String texto = comentario.getString("texto");
                Comentario c = new Comentario();
                c.setId(id);
                c.setUsuario_id(usuario_id);
                c.setPost_id(post_id);
                c.setTexto(texto);
                listaComentarios.add( c );
            }
            adapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getComentarios(){
        final ProgressDialog ringProgressDialog = ProgressDialog.show(getContext(), "Aguarde ...", "Carregando comentários ...", true);
        ringProgressDialog.setCancelable(true);
        String url = "http://noiadigital.com.br/spotted/public/api/post/comentarios";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Result handling

                        try {

                            adapter.clear();

                            JSONObject json = new JSONObject( response );
                            String status = json.getString("status");
                            JSONArray posts = json.getJSONArray("comentarios");

                            listaComentarios.clear();

                            for (int i = 0; i < posts.length(); i++) {
                                JSONObject post = posts.getJSONObject(i);
                                Integer id = (int) post.get("id");
                                int usuario_id = post.getInt("usuario_id");
                                int comentario_id = post.getInt("comentario_id");
                                int post_id = post.getInt("post_id");
                                String texto = post.getString("texto");
                                String data = post.getString("created_at");
                                int likes = post.getInt("likes");
                                int dislikes = post.getInt("dislikes");
                                boolean liked = post.getBoolean("liked");
                                boolean disliked = post.getBoolean("disliked");
                                Comentario c = new Comentario();
                                c.setId(id);
                                c.setUsuario_id(usuario_id);
                                c.setComentario_id(comentario_id);
                                c.setPost_id(post_id);
                                c.setTexto(texto);
                                c.setLikes(likes);
                                c.setDislikes(dislikes);
                                c.setLiked(liked);
                                c.setDisliked(disliked);

                                listaComentarios.add( c );
                            }
                            adapter.notifyDataSetChanged();
                            ringProgressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ringProgressDialog.hide();
                new AlertDialog.Builder(getContext())
                    .setTitle("Erro!")
                    .setMessage("Não foi possível conectar ao servidor.")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                        }
                    })
                    .show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                params.put("usuario_id", usuario_id );
                params.put("post_id", post_id );

                return params;
            }
        };

        // Add the request to the queue
        Volley.newRequestQueue(getContext()).add(stringRequest);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


}
